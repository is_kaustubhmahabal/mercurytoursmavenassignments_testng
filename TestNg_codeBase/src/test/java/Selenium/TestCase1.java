package Selenium.MavenProjectAssignment;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TestCase1 {
	WebDriver driver;
	VerifyHomePage vhomepage;
	SignIn login;
	SignOut logout;
	VerifyContactLink conlink;
	InvalidLogin invalidlogin;
	
	@BeforeTest
	public void LaunchWebsite(){
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver_win32\\chromedriver.exe");
		driver= new ChromeDriver();
		driver.get("http://newtours.demoaut.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	@Test(enabled = false)
	public void test_Home_Page_Appear_Correct() {
		vhomepage= new VerifyHomePage(driver);
		
		vhomepage.VerifyTopMenu();
		
		vhomepage.VerifyLeftMenu();
		
		vhomepage.VerifyHomePageMenu();
		
	}
	
	@Test(priority=0)
	public void SignIn() throws IOException {
		login= new SignIn(driver);
		
		login.Login();
		
	}
	

	@Test(priority=1)
	public void SignOut() throws IOException {
		logout= new SignOut(driver);
		
		logout.Logout();
		
	}
	

	@Test(enabled = false)
	public void VerifyTopMenuLink() {
		conlink = new VerifyContactLink(driver);
		conlink.VerifyTopMenuLink();
		
		
	}
	
	@Test(priority=2)
	@Parameters({ "username1", "password1" })
	public void CheckInvalidLogin(String username1, String password1) throws IOException {
		invalidlogin= new InvalidLogin(driver);
		
		invalidlogin.VerifyInvalidLogin1(username1,password1);
	}
	
}


