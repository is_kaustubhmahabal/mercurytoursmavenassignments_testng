package Selenium.MavenProjectAssignment;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Parameters;

public class InvalidLogin {
	WebDriver driver;

	
	public InvalidLogin(WebDriver driver)
	{
	this.driver=driver;
	}
	
	


	public void VerifyInvalidLogin1(String username1, String password1) throws IOException {
		
		driver.findElement(By.xpath("//input[@name='userName']")).sendKeys(username1);
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(password1);
		driver.findElement(By.xpath("//input[@value='Login']")).click();
		
		String pageTitle=driver.getTitle(); 
		
	    if(pageTitle.contains("Sign-on: Mercury Tours"))
	    System.out.println("Invalid Login");
	    else
	    	System.out.println("Wrong Operation");
	}
}
