package Selenium.MavenProjectAssignment;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class VerifyContactLink {
	WebDriver driver;
	public VerifyContactLink(WebDriver driver)
	{
	this.driver=driver;
	}
	
	public void VerifyTopMenuLink() {
		driver.findElement(By.xpath("//a[contains(text(),'CONTACT')]")).click();
		
		String conlink= driver.findElement(By.xpath("//font[@face='Arial, Helvetica, sans-serif' and @size='2']/b")).getText();
		System.out.println(conlink);
		if(conlink.contains("This section of our web site is currently under construction.   Sorry for any inconvienece."))
			System.out.println("Conatct Link is Verified");
		else
			System.out.println("Conatct Link is not Verified");
		
		driver.findElement(By.xpath("//img[@src='/images/forms/home.gif']")).click();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		
		String pageTitle=driver.getTitle(); 
		if(pageTitle.contains("Welcome: Mercury Tours"))
		    System.out.println("Returned to Home Page Successfully");
		else
			System.out.println("Not returned to Home Page");
	}
}
