package Selenium.MavenProjectAssignment;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class VerifyHomePage {
	WebDriver driver;
	
	By homePageMenu1= By.xpath("//img[@src='/images/featured_destination.gif']");
	By homePageMenu2= By.xpath("//img[@src='/images/hdr_specials.gif']");
	By homePageMenu3= By.xpath("//img[@src='/images/hdr_tips.gif']");
	By homePageMenu4= By.xpath("//img[@src='/images/hdr_findflight.gif']");
	By homePageMenu5= By.xpath("//img[@src='/images/hdr_destinations.gif']");
	By homePageMenu6= By.xpath("//img[@src='/images/hdr_vacation.gif']");
	By homePageMenu7= By.xpath("//img[@src='/images/hdr_register.gif']");
	By homePageMenu8= By.xpath("//img[@src='/images/hdr_links.gif']");
	
	By homePageLeftMenu1= By.xpath("//a[contains(text(),'Home')]");
	By homePageLeftMenu2= By.xpath("//a[contains(text(),'Flights')]");
	By homePageLeftMenu3= By.xpath("//a[contains(text(),'Hotels')]");
	By homePageLeftMenu4= By.xpath("//a[contains(text(),'Car Rentals')]");
	By homePageLeftMenu5= By.xpath("//a[contains(text(),'Cruises')]");
	By homePageLeftMenu6= By.xpath("//a[contains(text(),'Destinations')]");
	By homePageLeftMenu7= By.xpath("//a[contains(text(),'Vacations')]");
	
	By homePageTopMenu1=  By.xpath("//a[contains(text(),'SIGN-ON')]");
	By homePageTopMenu2=  By.xpath("//a[contains(text(),'REGISTER')]");
	By homePageTopMenu3=  By.xpath("//a[contains(text(),'SUPPORT')]");
	By homePageTopMenu4=  By.xpath("//a[contains(text(),'CONTACT')]");
	
	public VerifyHomePage(WebDriver driver)
	{
	this.driver=driver;
	}
	 
	public void VerifyTopMenu() {
		String expected1="SIGN-ON",expected2="REGISTER",expected3="SUPPORT",
		expected4="CONTACT";
		
		String actual1 = driver.findElement(homePageTopMenu1).getText();
		String actual2 = driver.findElement(homePageTopMenu2).getText();
		String actual3 = driver.findElement(homePageTopMenu3).getText();
		String actual4 = driver.findElement(homePageTopMenu4).getText();
		
		if (actual1.equals(expected1))
			System.out.println("top menu1 is present: "+actual1);
		else
			System.out.println("top menu1 is not present: "+actual1);
		
		if (actual2.equals(expected2))
			System.out.println("top menu2 is present: "+actual2);
		else
			System.out.println("top menu2 is not present: "+actual2);
		
		if (actual3.equals(expected3))
			System.out.println("top menu3 is present: "+actual3);
		else
			System.out.println("top menu3 is not present: "+actual3);
		
		if (actual4.equals(expected4))
			System.out.println("top menu4 is present: "+actual4);
		else
			System.out.println("top menu4 is not present: "+actual4);
	}
	
	public void VerifyLeftMenu() {
		String expected1="Home",expected2="Flights",expected3="Hotels",expected4="Car Rentals",
		expected5="Cruises",expected6="Destinations",expected7="Vacations";
		
		String actual1 = driver.findElement(homePageLeftMenu1).getText();
		String actual2 = driver.findElement(homePageLeftMenu2).getText();
		String actual3 = driver.findElement(homePageLeftMenu3).getText();
		String actual4 = driver.findElement(homePageLeftMenu4).getText();
		String actual5 = driver.findElement(homePageLeftMenu5).getText();
		String actual6 = driver.findElement(homePageLeftMenu6).getText();
		String actual7 = driver.findElement(homePageLeftMenu7).getText();
		
		if (actual1.equals(expected1))
			System.out.println("LeftMenu1 is present: "+actual1);
		else
			System.out.println("LeftMenu1 is not present: "+actual1);
		
		if (actual2.equals(expected2))
			System.out.println("LeftMenu2 is present: "+actual2);
		else
			System.out.println("LeftMenu2 is not present: "+actual2);
		
		if (actual3.equals(expected3))
			System.out.println("LeftMenu3 is present: "+actual3);
		else
			System.out.println("LeftMenu3 is not present: "+actual3);
		
		if (actual4.equals(expected4))
			System.out.println("LeftMenu4 is present: "+actual4);
		else
			System.out.println("LeftMenu4 is not present: "+actual4);
		
		if (actual5.equals(expected5))
			System.out.println("LeftMenu5 is present: "+actual5);
		else
			System.out.println("LeftMenu5 is not present: "+actual5);
		
		if (actual6.equals(expected6))
			System.out.println("LeftMenu6 is present: "+actual6);
		else
			System.out.println("LeftMenu6 is not present: "+actual6);
		
		if (actual7.equals(expected7))
			System.out.println("LeftMenu7 is present: "+actual7);
		else
			System.out.println("LeftMenu7 is not present: "+actual7);
	}
	
	
	public void VerifyHomePageMenu() {
		Boolean iselementpresent1 = driver.findElement(homePageMenu1).isDisplayed();
		Boolean iselementpresent2 = driver.findElement(homePageMenu2).isDisplayed();
		Boolean iselementpresent3 = driver.findElement(homePageMenu3).isDisplayed();
		Boolean iselementpresent4 = driver.findElement(homePageMenu4).isDisplayed();
		Boolean iselementpresent5 = driver.findElement(homePageMenu5).isDisplayed();
		Boolean iselementpresent6 = driver.findElement(homePageMenu6).isDisplayed();
		Boolean iselementpresent7 = driver.findElement(homePageMenu7).isDisplayed();
		Boolean iselementpresent8 = driver.findElement(homePageMenu8).isDisplayed();
		
		
		if(iselementpresent1==true)
			System.out.println("featured_destination is present");
		else
			System.out.println("featured_destination is not present");
		
		if(iselementpresent2==true)
			System.out.println("specials feature is present");
		else
			System.out.println("specials feature is not present");
		
		if(iselementpresent3==true)
			System.out.println("tour tips are present");
		else
			System.out.println("tour tips are not present");
		
		if(iselementpresent4==true)
			System.out.println("findflight is present");
		else
			System.out.println("findflight is not present");
		
		if(iselementpresent5==true)
			System.out.println("destinations is present");
		else
			System.out.println("destinations is not present");
		
		if(iselementpresent6==true)
			System.out.println("vacation is present");
		else
			System.out.println("vacation is not present");
		
		if(iselementpresent7==true)
			System.out.println("register is present");
		else
			System.out.println("register is not present");
		
		if(iselementpresent8==true)
			System.out.println("links feature is present");
		else
			System.out.println("links feature are not present");
		
	}
	
}
