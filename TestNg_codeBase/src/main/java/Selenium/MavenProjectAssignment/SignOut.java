package Selenium.MavenProjectAssignment;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class SignOut {
	
WebDriver driver;
WebElement signoutbtn;
	
	public SignOut(WebDriver driver)
	{
	this.driver=driver;
	}
	
	public void Logout() {
		WebDriverWait wait=new WebDriverWait(driver, 20);
		signoutbtn= wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'SIGN-OFF')]")));
		signoutbtn.click();
		
		String pageTitle=driver.getTitle(); 
		
	    if(pageTitle.contains("Sign-on: Mercury Tours"))
	    System.out.println("Logged out of the application Successfully");
	    else
	    	System.out.println("Log out failed");
	}

}
