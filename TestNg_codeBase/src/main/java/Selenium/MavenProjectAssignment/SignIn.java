package Selenium.MavenProjectAssignment;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SignIn {
	WebDriver driver;
	
	public SignIn(WebDriver driver)
	{
	this.driver=driver;
	}
	
	public void Login() throws IOException {
		Properties prop = new Properties();
		
		FileInputStream ip = new FileInputStream("D://MavenProjectAssignment//Credentials.properties");

		prop.load(ip);
		
		driver.findElement(By.xpath("//input[@name='userName']")).sendKeys(prop.getProperty("Username"));
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(prop.getProperty("Password"));
		driver.findElement(By.xpath("//input[@value='Login']")).click();;
		
		String pageTitle=driver.getTitle(); 
	    if(pageTitle.contains("Find a Flight: Mercury Tours:"))
	    System.out.println("Logged in to the application Successfully");
	}
}
